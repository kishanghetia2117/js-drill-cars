// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
const getCarLotYears = carList => {
    if (carList.length === 0) return 'no inventory'
    const carYearList = [];
    for (let carId = 0; carId < carList.length; carId++){
        carYearList.push(carList[carId].car_year);
    }
    return carYearList;
}
module.exports = getCarLotYears;