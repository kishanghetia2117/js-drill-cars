// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
const getFilterCarOnMake = (carList) => {
    const filteredCarMakeList = [];
    for (let carId = 0; carId < carList.length; carId++){
        if (carList[carId].car_make === 'BMW' || carList[carId].car_make === 'Audi') {
            
            filteredCarMakeList.push(carList[carId]);
        }
    }
    return filteredCarMakeList;
}
module.exports = getFilterCarOnMake;