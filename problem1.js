// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
const specificCarInfo = (carList,id) => {
    for (let carId = 0; carId < carList.length; carId++) 
        if (carId === id-1) {
            const carInfo = carList[carId];
            return(`Car ${carInfo.id} is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`);
        }
    return 'Id not found'
}

module.exports = specificCarInfo;