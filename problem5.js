// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
const getFileterCarOnYear = (yearList,filterByYear) => {
    const filteredCarYearList = [];
    for (let year = 0; year < yearList.length; year++){
        if (yearList[year] < filterByYear) {
            filteredCarYearList.push(yearList[year]);
        }
    }
    return filteredCarYearList;
}
module.exports = getFileterCarOnYear;