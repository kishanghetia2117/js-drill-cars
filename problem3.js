// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
const getalplhaSortedCarModelList = carList =>{
    if (carList.length === 0) return 'no inventory'
    const carModelList  = [];
    for (let carId = 0; carId < carList.length; carId++){
        carModelList.push(carList[carId].car_model);
    }
    carModelList.sort();
    return carModelList;
}

module.exports = getalplhaSortedCarModelList;